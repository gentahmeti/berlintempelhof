package com.gent.berlintemplehofreview

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.Toolbar
import android.widget.TextView
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReviewsActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule<ReviewsActivity>(ReviewsActivity::class.java)

    @Test
    fun testThatToolbarShowsCorrectTitle() {
        onView(allOf(isAssignableFrom(TextView::class.java), withParent(isAssignableFrom(Toolbar::class.java))))
            .check(matches(withText(R.string.berlin_tempelhof)))
    }
}