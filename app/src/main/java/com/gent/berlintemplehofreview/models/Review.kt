package com.gent.berlintemplehofreview.models

data class Review(val rating: Double,
                  val title: String?,
                  val content: String,
                  val author: String,
                  val date: String)