package com.gent.berlintemplehofreview.network

enum class SortDirection(val value : String) {
    ASC("asc"), DESC("desc")
}