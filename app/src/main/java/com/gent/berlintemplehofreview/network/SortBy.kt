package com.gent.berlintemplehofreview.network

enum class SortBy(val value: String) {
    DATE("date_of_review"), RATING("rating")
}