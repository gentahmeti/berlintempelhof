package com.gent.berlintemplehofreview.network

enum class Filter(val value: String) {
    ALL(""),
    COUPLES("couple"),
    FAMILY("family"),
    SOLO_TRAVELLER("solo"),
    GROUP_OF_FRIENDS("friends")
}