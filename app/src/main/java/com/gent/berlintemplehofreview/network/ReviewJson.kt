package com.gent.berlintemplehofreview.network

import com.google.gson.annotations.SerializedName

data class ReviewJson(
    @SerializedName("status") val status: Boolean,
    @SerializedName("total_reviews_comments") val total: Int,
    @SerializedName("data") val data: List<Data>
) {

    data class Data(
        @SerializedName("review_id") val reviewId: Long,
        @SerializedName("rating") val rating: Double,
        @SerializedName("title") val title: String?,
        @SerializedName("message") val message: String,
        @SerializedName("author") val author: String,
        @SerializedName("date") val date: String
    )
}