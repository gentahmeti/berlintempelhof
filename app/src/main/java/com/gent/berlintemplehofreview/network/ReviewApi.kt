package com.gent.berlintemplehofreview.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ReviewApi {

    @GET("berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json")
    @Headers("user-agent: GetYourGuide")
    fun getReviews(
        @Query("count") count: Int,
        @Query("page") page: Int,
        @Query("type") type: String,
        @Query("sortBy") sortBy: String,
        @Query("direction") direction: String
    ) : Observable<ReviewJson>
}