package com.gent.berlintemplehofreview.di

import com.gent.berlintemplehofreview.BuildConfig
import com.gent.berlintemplehofreview.network.ReviewApi
import com.gent.berlintemplehofreview.repositories.NetworkRepository
import com.gent.berlintemplehofreview.usecases.GetAllReviewsUseCase
import com.gent.berlintemplehofreview.viewmodels.ReviewViewModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceLocator private constructor() {

    companion object {

        private var instance: ServiceLocator = ServiceLocator()

        fun getInstance() = instance
    }

    val reviewApi = lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return@lazy retrofit.create(ReviewApi::class.java)
    }

    val reviewRepository = NetworkRepository(reviewApi.value)

    val useCase = GetAllReviewsUseCase(reviewRepository)

    val reviewViewModel = ReviewViewModel(useCase)
}