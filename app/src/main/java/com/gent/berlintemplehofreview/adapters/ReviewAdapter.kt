package com.gent.berlintemplehofreview.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gent.berlintemplehofreview.R
import com.gent.berlintemplehofreview.models.Review
import com.gent.berlintemplehofreview.views.RatingView

class ReviewAdapter : RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    private val reviews: MutableList<Review> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.view_review, parent, false))
    }

    override fun getItemCount(): Int = reviews.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
        viewHolder.fillData(reviews[position])

    fun addItems(reviews: List<Review>) {
        val currentSize = this.reviews.size
        this.reviews.addAll(reviews)
        notifyItemRangeInserted(currentSize, reviews.size)
    }

    fun clear() {
        this.reviews.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title: TextView = itemView.findViewById(R.id.review_title)
        private val content: TextView = itemView.findViewById(R.id.review_content)
        private val date: TextView = itemView.findViewById(R.id.review_date)
        private val author: TextView = itemView.findViewById(R.id.review_by)
        private val rating: RatingView = itemView.findViewById(R.id.review_rating)

        fun fillData(review: Review) {
            title.text = review.title
            content.text = review.content
            date.text = review.date
            author.text = review.author
            rating.setStars(review.rating)
        }
    }
}