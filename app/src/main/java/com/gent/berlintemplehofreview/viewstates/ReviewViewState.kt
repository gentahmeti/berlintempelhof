package com.gent.berlintemplehofreview.viewstates

import com.gent.berlintemplehofreview.models.Review

sealed class ReviewViewState

data class ReviewsReceived(val reviews: List<Review>) : ReviewViewState()

data class ReviewError(val error: String) : ReviewViewState()