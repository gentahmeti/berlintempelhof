package com.gent.berlintemplehofreview.usecases

import com.gent.berlintemplehofreview.models.Review
import com.gent.berlintemplehofreview.network.Filter
import com.gent.berlintemplehofreview.network.SortBy
import com.gent.berlintemplehofreview.network.SortDirection
import io.reactivex.Observable

interface GetReviewsUseCase {

    fun getReviews(page: Int, sortBy: SortBy, direction: SortDirection, filter: Filter) : Observable<List<Review>>
}