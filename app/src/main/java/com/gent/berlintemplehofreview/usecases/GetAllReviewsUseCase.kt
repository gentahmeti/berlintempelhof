package com.gent.berlintemplehofreview.usecases

import com.gent.berlintemplehofreview.models.Review
import com.gent.berlintemplehofreview.network.Filter
import com.gent.berlintemplehofreview.network.SortBy
import com.gent.berlintemplehofreview.network.SortDirection
import com.gent.berlintemplehofreview.repositories.ReviewRepository
import io.reactivex.Observable

private const val PAGE_SIZE = 5

class GetAllReviewsUseCase(private val reviewRepository: ReviewRepository) : GetReviewsUseCase {

    override fun getReviews(page: Int, sortBy: SortBy, direction: SortDirection, filter: Filter) =
        reviewRepository.getReviews(
            page,
            PAGE_SIZE,
            sortBy,
            direction,
            filter
        )
            .flatMap { Observable.fromIterable(it.data) }
            .map { Review(it.rating, it.title, it.message, it.author, it.date) }
            .toList()
            .toObservable()
}