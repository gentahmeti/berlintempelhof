package com.gent.berlintemplehofreview.viewmodels

import com.gent.berlintemplehofreview.network.Filter
import com.gent.berlintemplehofreview.network.SortBy
import com.gent.berlintemplehofreview.network.SortDirection
import com.gent.berlintemplehofreview.usecases.GetReviewsUseCase
import com.gent.berlintemplehofreview.viewstates.ReviewError
import com.gent.berlintemplehofreview.viewstates.ReviewViewState
import com.gent.berlintemplehofreview.viewstates.ReviewsReceived
import io.reactivex.Observable

class ReviewViewModel(private val getReviewsUseCase: GetReviewsUseCase) {

    private var isLoading = false
    private var page = 0

    var sortBy = SortBy.RATING
        set(value) {
            page = 0
            field = value
        }
    var sortDirection = SortDirection.DESC
        set(value) {
            page = 0
            field = value
        }
    var filter = Filter.ALL
        set(value) {
            page = 0
            field = value
        }

    fun getReviews(): Observable<ReviewViewState> =
            if (isLoading) {
                Observable.empty()
            } else {
                isLoading = true
                getReviewsUseCase.getReviews(page++, sortBy, sortDirection, filter)
                    .map { ReviewsReceived(it) as ReviewViewState }
                    .onErrorReturn { ReviewError(it.message?: "Something went wrong") }
                    .doOnNext { isLoading = false }
            }
}