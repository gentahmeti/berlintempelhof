package com.gent.berlintemplehofreview.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.gent.berlintemplehofreview.R
import kotlinx.android.synthetic.main.view_rating.view.*

class RatingView : ConstraintLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        View.inflate(context, R.layout.view_rating, this)
    }

    fun setStars(stars: Double) {
        unFilledStar1.visibility = if (stars >= 1) View.GONE else View.VISIBLE
        unFilledStar2.visibility = if (stars >= 2) View.GONE else View.VISIBLE
        unFilledStar3.visibility = if (stars >= 3) View.GONE else View.VISIBLE
        unFilledStar4.visibility = if (stars >= 4) View.GONE else View.VISIBLE
        unFilledStar5.visibility = if (stars >= 5) View.GONE else View.VISIBLE
    }
}