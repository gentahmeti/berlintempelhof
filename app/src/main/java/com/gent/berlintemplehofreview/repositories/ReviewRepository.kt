package com.gent.berlintemplehofreview.repositories

import com.gent.berlintemplehofreview.network.Filter
import com.gent.berlintemplehofreview.network.ReviewJson
import com.gent.berlintemplehofreview.network.SortBy
import com.gent.berlintemplehofreview.network.SortDirection
import io.reactivex.Observable

interface ReviewRepository {

    fun getReviews(
        page: Int,
        size: Int,
        sortBy: SortBy,
        direction: SortDirection,
        filter: Filter
    ): Observable<ReviewJson>
}