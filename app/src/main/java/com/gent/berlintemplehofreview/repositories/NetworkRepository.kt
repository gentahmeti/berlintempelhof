package com.gent.berlintemplehofreview.repositories

import com.gent.berlintemplehofreview.network.*
import io.reactivex.Observable

class NetworkRepository(private val reviewApi: ReviewApi) : ReviewRepository {

    override fun getReviews(
        page: Int,
        size: Int,
        sortBy: SortBy,
        direction: SortDirection,
        filter: Filter
    ): Observable<ReviewJson> =
        reviewApi.getReviews(size, page, filter.value, sortBy.value, direction.value)
}