package com.gent.berlintemplehofreview.util

import android.support.v7.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent
import io.reactivex.functions.Predicate

class LoadMoreFilter(val layoutManager: LinearLayoutManager) : Predicate<RecyclerViewScrollEvent> {

    override fun test(t: RecyclerViewScrollEvent): Boolean {
        val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
        val totalItemCount = layoutManager.itemCount
        val visibleThreshold = 5

        return (lastVisiblePosition + visibleThreshold) > totalItemCount
    }
}