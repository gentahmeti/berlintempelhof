package com.gent.berlintemplehofreview

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.gent.berlintemplehofreview.adapters.ReviewAdapter
import com.gent.berlintemplehofreview.di.ServiceLocator
import com.gent.berlintemplehofreview.network.Filter
import com.gent.berlintemplehofreview.network.SortBy
import com.gent.berlintemplehofreview.network.SortDirection
import com.gent.berlintemplehofreview.util.LoadMoreFilter
import com.gent.berlintemplehofreview.viewstates.ReviewError
import com.gent.berlintemplehofreview.viewstates.ReviewsReceived
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_reviews.*
import kotlinx.android.synthetic.main.content_reviews.*

class ReviewsActivity : AppCompatActivity() {

    private val reviewsAdapter = ReviewAdapter()
    private val viewModel = ServiceLocator.getInstance().reviewViewModel
    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)
        setSupportActionBar(toolbar)

        val layoutManager = LinearLayoutManager(this)
        reviews.layoutManager = layoutManager
        reviews.adapter = reviewsAdapter

        val dividerItemDecoration = DividerItemDecoration(
            this,
            LinearLayoutManager.VERTICAL
        )
        reviews.addItemDecoration(dividerItemDecoration)

        disposables.addAll(
            viewModel.getReviews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        when (it) {
                            is ReviewsReceived -> reviewsAdapter.addItems(it.reviews)
                            is ReviewError -> showSnackbar(it.error)
                        }
                    },
                    { error -> showSnackbar(error.message ?: "Something went wrong") }
                ),
            RxRecyclerView.scrollEvents(reviews)
                .observeOn(Schedulers.io())
                .filter(LoadMoreFilter(layoutManager))
                .flatMap { viewModel.getReviews() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        when (it) {
                            is ReviewsReceived -> reviewsAdapter.addItems(it.reviews)
                            is ReviewError -> showSnackbar(it.error)
                        }
                    },
                    { error -> showSnackbar(error.message ?: "Something went wrong") }
                )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_reviews, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_sort_by_rating -> viewModel.sortBy = SortBy.RATING
            R.id.action_sort_by_date -> viewModel.sortBy = SortBy.DATE
            R.id.action_sort_by_asc -> viewModel.sortDirection = SortDirection.ASC
            R.id.action_sort_by_desc -> viewModel.sortDirection = SortDirection.DESC
            R.id.action_filter_all -> viewModel.filter = Filter.ALL
            R.id.action_filter_couples -> viewModel.filter = Filter.COUPLES
            R.id.action_filter_family -> viewModel.filter = Filter.FAMILY
            R.id.action_filter_group_of_friends -> viewModel.filter = Filter.GROUP_OF_FRIENDS
            R.id.action_filter_solo_traveller -> viewModel.filter = Filter.SOLO_TRAVELLER
        }

        item?.let {
            if (it.itemId != R.id.action_sort_by && it.itemId != R.id.action_filter
                && it.itemId != R.id.action_sort_by_direction
            ) {
                reviewsAdapter.clear()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showSnackbar(text: String = "Something went wrong") =
        Snackbar.make(reviews, text, Snackbar.LENGTH_LONG).show()
}
