package com.gent.berlintemplehofreview

import com.gent.berlintemplehofreview.network.ReviewJson
import com.gent.berlintemplehofreview.repositories.ReviewRepository
import com.gent.berlintemplehofreview.usecases.GetAllReviewsUseCase
import com.gent.berlintemplehofreview.viewmodels.ReviewViewModel
import com.gent.berlintemplehofreview.viewstates.ReviewError
import com.gent.berlintemplehofreview.viewstates.ReviewsReceived
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ViewModelTest {

    @Test
    fun testThatViewModelGetsCorrectlyMappedJson() {
        val repository = mock<ReviewRepository> {
            whenever(it.getReviews(any(), any(), any(), any(), any())).thenReturn(
                Observable.just(
                    ReviewJson(
                        true,
                        1,
                        listOf(
                            ReviewJson.Data(
                                1,
                                4.0,
                                "",
                                "",
                                "",
                                ""
                            )
                        )
                    )
                )
            )
        }

        val viewModel = ReviewViewModel(GetAllReviewsUseCase(repository))

        viewModel.getReviews()
            .test()
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue { it is ReviewsReceived }
            .assertValue {
                with((it as ReviewsReceived).reviews[0]) {
                    rating == 4.0 && title.equals("") && content == "" && author == "" && date == ""
                }
            }
    }

    @Test
    fun testWhenRepositoryThrowsAnErrorItPropagatesToViewModel() {
        val repository = mock<ReviewRepository> {
            whenever(it.getReviews(any(), any(), any(), any(), any())).thenReturn(
                Observable.error(RuntimeException("Generic error"))
            )
        }

        val viewModel = ReviewViewModel(GetAllReviewsUseCase(repository))

        viewModel.getReviews()
            .test()
            .assertNoErrors()
            .assertValue { it is ReviewError }
            .assertValue {
                (it as ReviewError).error == "Generic error"
            }
    }
}