### Summary

This project shows the reviews for one of the most popular tourist attractions on GetYourGuide in Berlin.
It lists all reviews on a simple Activity with options to sort and filter. 

### How to build

The app is a simple Android app, it suffices to open the project in Android Studio and run it on a device or emulator.

### Architecture and patterns

For this project I'm using the MVVM architecture with reactive extensions. I'm using MVVM because of the clear
separation of the ViewModel and View, we only expose an Observable of the model and it doesn't matter who's
subscribing on the other hand. 
On other patterns there is a bigger connection between the view and business logic. In MVP it's good if you use
an interface to abstract out the View, but you will still have a reference to the View on the presenter.

I'm also using the Repository pattern to abstract how we get the data. I'm only using network calls for now,
but it can easily be changed to cache data before making a new request.

I am also using UseCases to further separate the ViewModel from the Repositories. In this project we only have a single 
UseCase, which is to retrieve all reviews.

I'm not using any DI library, like Dagger2, because we have only a few classes which would benefit from it, and
the overhead of adding Dagger2 isn't worth it. I'm using the Service Locator pattern, by providing dependencies
to view models and repositories through their constructors. I prefer to use an actual DI library like Dagger2, KOIN or Kodein
but I decided it would be simpler and more appropriate to use this approach on this particular project.